/*
 * Copyright (C) 2020-2023. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.boostkit.omniadvisor.utils;

public final class MathUtils {
    public static final long SECOND_IN_MS = 1000L;
    public static final long MINUTE_IN_MS = 60L * SECOND_IN_MS;
    public static final long HOUR_IN_MS = 60L * MINUTE_IN_MS;
    public static final long DAY_IN_MS = 24 * HOUR_IN_MS;

    private MathUtils() {}
}

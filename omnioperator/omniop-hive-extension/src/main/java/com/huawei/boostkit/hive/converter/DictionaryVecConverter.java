package com.huawei.boostkit.hive.converter;

import nova.hetu.omniruntime.vector.Decimal128Vec;
import nova.hetu.omniruntime.vector.DictionaryVec;
import nova.hetu.omniruntime.vector.Vec;

import org.apache.hadoop.hive.common.type.Date;
import org.apache.hadoop.hive.common.type.HiveDecimal;
import org.apache.hadoop.hive.common.type.Timestamp;
import org.apache.hadoop.hive.serde2.lazy.ByteArrayRef;
import org.apache.hadoop.hive.serde2.lazy.LazyHiveChar;
import org.apache.hadoop.hive.serde2.lazy.LazyHiveVarchar;
import org.apache.hadoop.hive.serde2.lazy.LazyString;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyHiveCharObjectInspector;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyHiveVarcharObjectInspector;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyStringObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.DecimalTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.PrimitiveTypeInfo;

public class DictionaryVecConverter implements VecConverter {
    @Override
    public Object fromOmniVec(Vec vec, int index, PrimitiveObjectInspector primitiveObjectInspector) {
        if (vec.isNull(index)) {
            return null;
        }
        DictionaryVec dictionaryVec = (DictionaryVec) vec;
        PrimitiveTypeInfo primitiveTypeInfo = primitiveObjectInspector.getTypeInfo();
        Object result = null;
        switch (primitiveTypeInfo.getPrimitiveCategory()) {
            case BYTE:
                result = (byte) dictionaryVec.getShort(index);
                break;
            case SHORT:
                result = dictionaryVec.getShort(index);
                break;
            case INT:
                result = dictionaryVec.getInt(index);
                break;
            case LONG:
                result = dictionaryVec.getLong(index);
                break;
            case BOOLEAN:
                result = dictionaryVec.getBoolean(index);
                break;
            case FLOAT:
            case DOUBLE:
                result = dictionaryVec.getDouble(index);
                break;
            case STRING: {
                byte[] bytes = dictionaryVec.getBytes(index);
                LazyString lazyString = new LazyString((LazyStringObjectInspector) primitiveObjectInspector);
                ByteArrayRef byteArrayRef = new ByteArrayRef();
                byteArrayRef.setData(bytes);
                lazyString.init(byteArrayRef, 0, bytes.length);
                result = lazyString;
                break;
            }
            case VARCHAR: {
                byte[] bytes = dictionaryVec.getBytes(index);
                LazyHiveVarchar lazyHiveVarchar = new LazyHiveVarchar((LazyHiveVarcharObjectInspector)
                        primitiveObjectInspector);
                ByteArrayRef byteArrayRefVarchar = new ByteArrayRef();
                byteArrayRefVarchar.setData(bytes);
                lazyHiveVarchar.init(byteArrayRefVarchar, 0, bytes.length);
                result = lazyHiveVarchar;
                break;
            }
            case CHAR: {
                byte[] bytes = dictionaryVec.getBytes(index);
                LazyHiveChar lazyHiveChar = new LazyHiveChar((LazyHiveCharObjectInspector) primitiveObjectInspector);
                ByteArrayRef byteArrayRefChar = new ByteArrayRef();
                byteArrayRefChar.setData(bytes);
                lazyHiveChar.init(byteArrayRefChar, 0, bytes.length);
                result = lazyHiveChar;
                break;
            }
            case TIMESTAMP:
                result = Timestamp.ofEpochMilli(dictionaryVec.getLong(index));
                break;
            case DATE:
                result = Date.ofEpochMilli(dictionaryVec.getLong(index));
                break;
            case DECIMAL: {
                DecimalTypeInfo decimalTypeInfo = (DecimalTypeInfo) primitiveTypeInfo;
                result = HiveDecimal.create(Decimal128Vec.getDecimal(dictionaryVec.getDecimal128(index)),
                        decimalTypeInfo.getScale());
                break;
            }
            default:
                break;
        }
        return result;
    }

    @Override
    public Vec toOmniVec(Object[] col, int columnSize) {
        throw new RuntimeException(String.format("%s doesn't support toOmniVecV2", this.getClass().getSimpleName()));
    }
}
